//! An animated solar system.
//!
//! This example showcases how to use a `Canvas` widget with transforms to draw
//! using different coordinate systems.
//!
//! Inspired by the example found in the MDN docs[1].
//!
//! [1]: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Basic_animations#An_animated_solar_system
use iced::{
    canvas::{self, Cursor, Path, Stroke},
    executor, time, window, Application, Canvas, Color, Command, Element, Length, Point, Rectangle,
    Settings, Subscription,
};

use std::time::Instant;

pub fn main() {
    Hail::run(Settings {
        antialiasing: true,
        ..Settings::default()
    })
}

struct Hail {
    state: State,
}

#[derive(Debug, Clone, Copy)]
enum Message {
    Tick(Instant),
}

impl Application for Hail {
    type Executor = executor::Default;
    type Message = Message;
    type Flags = ();

    fn new(_flags: ()) -> (Self, Command<Message>) {
        (
            Hail {
                state: State::new(),
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("hailview")
    }

    fn subscription(&self) -> Subscription<Message> {
        time::every(std::time::Duration::from_millis(10)).map(|instant| Message::Tick(instant))
    }

    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            Message::Tick(instant) => {
                self.state.update(instant);
            }
        }

        Command::none()
    }

    fn view(&mut self) -> Element<Message> {
        Canvas::new(&mut self.state)
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }
}

#[derive(Debug)]
pub struct Dot {
    x: Point,
    v: [f32; 2],
}

impl Dot {
    pub fn tick(&mut self, dt: f32, gamma: f32) {
        self.x.x += self.v[0] * dt;
        self.x.y += self.v[1] * dt;
        self.v[0] *= gamma;
        self.v[1] *= gamma;
    }
}

#[derive(Debug)]
struct State {
    now: Instant,
    dot_cache: canvas::Cache,
    dots: Vec<Dot>,
}

impl State {
    const K: f32 = 1.0 / 3.0;
    const L: f32 = 300.0;
    const N: usize = 30;

    pub fn new() -> State {
        let now = Instant::now();
        let (width, height) = window::Settings::default().size;
        let dots = Self::generate_dots(width, height);

        State {
            now: now,
            dot_cache: Default::default(),
            dots,
        }
    }

    pub fn update(&mut self, now: Instant) {
        let dt = (now - self.now).as_secs_f32();
        self.now = now;
        self.dot_cache.clear();
        for i in 0..self.dots.len() {
            for j in 0..self.dots.len() {
                if i == j {
                    continue;
                }
                let d = self.dots[i].x.distance(self.dots[j].x);
                let particle_displacement = [
                    (self.dots[j].x.x - self.dots[i].x.x),
                    (self.dots[j].x.y - self.dots[i].x.y),
                ];
                let direction = [particle_displacement[0] / d, particle_displacement[1] / d];
                let spring_equilibrium = [direction[0] * Self::L, direction[1] * Self::L];
                let spring_displacement = [
                    (particle_displacement[0] - spring_equilibrium[0]),
                    (particle_displacement[1] - spring_equilibrium[1]),
                ];
                let a = [
                    Self::K * spring_displacement[0],
                    Self::K * spring_displacement[1],
                ];
                self.dots[i].v[0] += dt * a[0];
                self.dots[i].v[1] += dt * a[1];
            }
        }
        for dot in self.dots.iter_mut() {
            dot.tick(dt, 0.99)
        }
    }

    pub fn generate_dots(width: u32, height: u32) -> Vec<Dot> {
        use rand::Rng;
        let mut rng = rand::thread_rng();
        (0..Self::N)
            .map(|_| Dot {
                x: Point::new(
                    rng.gen_range(0.0, width as f32),
                    rng.gen_range(0.0, height as f32),
                ),
                v: [rng.gen_range(0.1, 1.0), rng.gen_range(0.1, 1.0)],
            })
            .collect()
    }

    pub fn draw_web(&self, frame: &mut canvas::Frame) {
        let web = Path::new(|path| {
            for x in &self.dots {
                for y in &self.dots {
                    if x.x != y.x {
                        path.move_to(x.x);
                        path.line_to(y.x);
                    }
                }
            }
        });
        frame.stroke(
            &web,
            Stroke {
                color: Color::new(0.0, 0.0, 0.0, 0.5),
                line_cap: canvas::LineCap::Butt,
                width: 1.0,
                line_join: canvas::LineJoin::Miter,
            },
        )
    }

    pub fn draw_dots(&self, frame: &mut canvas::Frame) {
        let dots = Path::new(|path| {
            for p in &self.dots {
                path.circle(p.x, 2.0)
            }
        });
        frame.fill(&dots, Color::BLACK)
    }
}

impl<Message> canvas::Program<Message> for State {
    fn draw(&self, bounds: Rectangle, _cursor: Cursor) -> Vec<canvas::Geometry> {
        let background = self.dot_cache.draw(bounds.size(), |frame| {
            self.draw_web(frame);
            self.draw_dots(frame)
        });
        vec![background]
    }
}
